---
layout: post
title:  "Bem-vindos ao belhote!"
date:   2015-02-20 20:22:30
categories: belhote podcast
---

<audio controls>
    <source src="{{ site.baseurl }}/assets/audio/podcast1.ogg" type="audio/ogg">
    <source src="{{ site.baseurl }}/assets/audio/podcast1.mp3" type="audio/mpeg">
    Your browser does not support the audio element.
</audio>

<p>Enviem-nos email com mensagens, cartas, declarações e o belhote irá transmiti-las nos próximos podcasts.</p>
